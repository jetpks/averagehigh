package weatherdata

import (
	"encoding/json"
	"fmt"
)

// WeatherData represents a collection of forecasts from a specific location
type WeatherData struct {
	LocationName string `json:"title"`
	Forecasts    []struct {
		MaxTemp float64 `json:"max_temp"`
	} `json:"consolidated_weather"`
}

// New creates a new WeatherData object from raw json
func New(raw []byte) (*WeatherData, error) {
	parsed := &WeatherData{}

	err := json.Unmarshal(raw, &parsed)
	if err != nil {
		return nil, fmt.Errorf("Problem unmarshaling JSON: %w", err)
	}

	return parsed, nil
}

// AverageMaxTemp returns the average maximum temperature from all available
// forecast data
func (w *WeatherData) AverageMaxTemp() (float64, error) {
	qtyForecasts := len(w.Forecasts)
	if qtyForecasts == 0 {
		return 0.0, fmt.Errorf("No forecast data available to average")
	}

	sum := 0.0
	for _, forecast := range w.Forecasts {
		sum += forecast.MaxTemp
	}

	average := sum / float64(len(w.Forecasts))

	return average, nil
}
