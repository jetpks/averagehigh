package main

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/jetpks/averagehigh/weatherdata"
)

var sources []string = []string{
	"https://www.metaweather.com/api/location/2487610/",
	"https://www.metaweather.com/api/location/2442047/",
	"https://www.metaweather.com/api/location/2366355/",
}

func main() {
	// Proper solution (now with more concurrency)
	//
	// 1. break naive solution into functions DONE
	// 2. have the functions read and write to channels DONE
	// 3. setup concurrency DONE

	qtySources := len(sources)

	urls := make(chan string)
	rawJSON := make(chan []byte)
	weatherDatas := make(chan *weatherdata.WeatherData)
	done := make(chan struct{})

	go getURL(urls, rawJSON)
	go parseRaw(rawJSON, weatherDatas)
	go printResult(weatherDatas, done)

	for _, url := range sources {
		urls <- url
	}

	dones := 0
	for range done {
		dones++
		if dones == qtySources {
			close(done)
			close(urls)
		}
	}
}

func printResult(weatherDatas <-chan *weatherdata.WeatherData, done chan<- struct{}) error {
	for {
		select {
		case wd, ok := <-weatherDatas:
			if !ok {
				// Closing down
				return nil
			}

			average, err := wd.AverageMaxTemp()
			if err != nil {
				return fmt.Errorf("Could not formulate average: %w", err)
			}

			fmt.Printf("%s Average Max Temp: %2f\n", wd.LocationName, average)
			done <- struct{}{}
		}
	}
}

func parseRaw(rawJSON <-chan []byte, weatherDatas chan<- *weatherdata.WeatherData) error {
	defer close(weatherDatas)
	for {
		select {
		case raw, ok := <-rawJSON:
			if !ok {
				// Closing down
				return nil
			}

			parsed, err := weatherdata.New(raw)
			if err != nil {
				return fmt.Errorf("Problem parsing weatherdata: %w", err)
			}

			weatherDatas <- parsed
		}
	}
}

func getURL(urls <-chan string, rawJSON chan<- []byte) error {
	defer close(rawJSON)
	for {
		select {
		case url, ok := <-urls:
			go func() error {
				if !ok {
					// Closing down
					return nil
				}

				resp, err := http.Get(url)
				if err != nil {
					return fmt.Errorf("Problem getting %s: %w", url, err)
				}
				defer resp.Body.Close()

				raw, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					return fmt.Errorf("Problem reading body: %w", err)
				}

				rawJSON <- raw
				return nil
			}()
		}
	}
}
